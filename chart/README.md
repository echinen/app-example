## Installing/Upgrading/Uninstalling the Chart
View in [README charts](../README.md)

## Configuration

The following table lists the configurable parameters of the `gpa-apis` chart and their default values.

| Parameter | Description   | Default   |
|-----------|---------------|-----------|
| `environment` | Set container environment variable `ENVIRONMENT`  | `local`   |
| `reservedInstanceGroup`   | Specifies nodeLabel for start pod in reserved IG  | `null` |
| `appPort` | Specifies application Port  | `80`    |
| `customNodePort`  | Specifies custom Node Port    | `{}`   |
| `customCommandJob`    | Custom Command for container  | `[]`  |
| `customArgsJob`   | Custom Args for container | `[]`  |
| `terminationGracePeriodSeconds`   | Specifies termination grace period in seconds | `30`  |
| `minReplicas` | Minimum number of pods    | 2 |
| `resources.limits.cpu`    | Specifies cpu limit   | `100m`    |
| `resources.limits.memory` | Specifies memory limit    | `100Mi`   |
| `resources.requests.cpu`  | Specifies initial cpu request | `50m` |
| `resources.requests.memory`   | Specifies initial memory request  | `50Mi`    |
| `arnIAMrole`  | Specifies AWS IAM Role (ARN) whether application needs integration with AWS services (via API calls)  | `null`    |
| `image.repository`    | Container repository  |  `null`   |
| `image.tag`   | Container tag |  `web` |
| `image.pullPolicy`    | Container pull policy | `Always`  |
| `extraEnv`    | Additional container environment variables    | `[]`  |
| `albHealthCheckPath`    | Specifies path health check for target group in ALB   | `/health` |
| `readinessProbe.httpHeaders` | Specifies HTTP headers readiness probe | `[]`  |
| `readinessProbe.path` | Specifies path readiness probe |  `/health-check/readiness`    |
| `readinessProbe.initialDelaySeconds`  | Delay before readiness probe is initiated | 5 |
| `readinessProbe.timeoutSeconds`   | When the probe times out   | 3  |
| `readinessProbe.periodSeconds`    | How often to perform the probe    | 10    |
| `readinessProbe.failureThreshold` | Minimum consecutive failures for the probe to be considered failed after having succeeded.    | 3 |
| `readinessProbe.successThreshold` | Minimum consecutive successes for the probe to be considered successful after having failed.  | 1 |
| `livenessProbe.httpHeaders` | Specifies HTTP headers liveness probe | `[]`  |
| `livenessProbe.path` | Specifies path liveness probe |  `/health-check/liveness`    |
| `livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated | 5 |
| `livenessProbe.timeoutSeconds`   | When the probe times out   | 3  |
| `livenessProbe.periodSeconds`    | How often to perform the probe    | 10    |
| `livenessProbe.failureThreshold` | Minimum consecutive failures for the probe to be considered failed after having succeeded.    | 3 |
| `livenessProbe.successThreshold` | Minimum consecutive successes for the probe to be considered successful after having failed.  | 1 |
| `hpa.maxReplicas` | Maximum number of pods    | 10    |
| `hpa.scalingBy` | Metric type to scale pods   | `cpu` |
| `hpa.targetAverageUtilization`    | Threshold in percent to scale pods    | `80` |
| `annotations` | Pod Annotations   | `[]` |
| `initContainers` | Init containers   | `[]` |
| `volumes` | Specifies Pod volumes    | `[]` |
| `volumeMounts` | Specifies where Kubernetes will mount Pod volumes   | `[]` |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm upgrade --install`.
