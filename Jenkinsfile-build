String awsEcrURL = "${env.AWS_ECR_URL}"
String awsEcrAccount = "${env.AWS_ECR_ACCOUNT}"
String project = "${env.PROJECT}"
String projectRepo = "${env.PROJECT_REPO}"
String envKey = "${env.ENV_KEY}"
String publishVersion = "${envKey}-${env.BUILD_ID}"

node {
    stage('Preparation') {
        checkout scm
    }
    stage('Build Docker Image') {
        sh "docker build -t ${projectRepo}:latest ."
    }
    stage('Push and Tag Docker Image') {
        String imageExists =  sh(returnStdout: true, script: "docker images -q ${projectRepo}:latest 2> /dev/null")
        if(imageExists != ""){
            sh "aws ecr get-login --region sa-east-1 --no-include-email --registry-ids ${awsEcrAccount} | sh -"
            sh "docker tag ${projectRepo}:latest ${projectRepo}:${publishVersion}"
            docker.withRegistry("https://${awsEcrURL}") {
                def image = docker.image("${projectRepo}:${publishVersion}")
                image.push()
                image.push(envKey)
            }
            sh("docker rmi -f ${projectRepo}:latest || :")
            sh("docker rmi -f ${projectRepo}:${publishVersion} || :")
            sh("docker rmi -f ${awsEcrURL}/${projectRepo}:${publishVersion} || :")
            sh("docker rmi -f ${awsEcrURL}/${projectRepo}:${envKey} || :")
            if(env.DEPLOY == 'true'){
                stage('Deploy') {
                    build job: "cda-${envKey}/deploy/${project}", parameters:
                    [
                        [$class: 'StringParameterValue', name: 'NODE_NAME', value: "${env.NODE_NAME}"]
                    ]
                }
            }
        }
    }
}